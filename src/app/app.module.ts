import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ErrorHandler, NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { IonicStorageModule } from '@ionic/storage';
import { RestaurantePageModule } from './../pages/restaurante/restaurante.module';
import { MiCuentaPageModule } from './../pages/mi-cuenta/mi-cuenta.module';
import { MenuPageModule } from './../pages/menu/menu.module';
import { ListaPageModule } from './../pages/lista/lista.module';
import { StartPageModule } from './../pages/start/start.module';
import { MapaPageModule } from '../pages/mapa/mapa.module';
import { FavoritosPageModule } from '../pages/favoritos/favoritos.module';
import { BuscarPageModule } from '../pages/buscar/buscar.module';
import { TabsPageModule } from '../pages/tabs/tabs.module';
import { InfoPageModule } from '../pages/info/info.module';
import { EditarPerfilPageModule } from '../pages/editar-perfil/editar-perfil.module';
import { DejarOpinionPageModule } from './../pages/dejar-opinion/dejar-opinion.module';
import { MyApp } from './app.component';
import { ServiceProvider } from '../providers/service/service';
import { AuthProvider } from '../providers/auth/auth';
import { RestaurantesProvider } from '../providers/restaurantes/restaurantes';
import { LocationProvider } from '../providers/location/location';
import { Geolocation } from '@ionic-native/geolocation';
import { ProfileProvider } from '../providers/profile/profile';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { QRScanner } from '@ionic-native/qr-scanner';
import { QrScannerPageModule } from '../pages/qr-scanner/qr-scanner.module';
import { LoginPageModule } from '../pages/login/login.module';
import { RegisterPageModule } from '../pages/register/register.module';
import { EmailComposer } from '@ionic-native/email-composer';
import { CallNumber } from '@ionic-native/call-number';
import { MapaProvider } from '../providers/mapa/mapa';
import { Facebook, } from '@ionic-native/facebook';
import { PrimeraVezPageModule } from '../pages/primera-vez/primera-vez.module';

@NgModule({
  declarations: [
    MyApp,
  ],
  imports: [
    IonicModule.forRoot(MyApp, {
      mode: 'md',
      tabsHideOnSubPages: true,
    }),
    BrowserAnimationsModule,
    ReactiveFormsModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
    BrowserModule,
    StartPageModule,
    MapaPageModule,
    ListaPageModule,
    PrimeraVezPageModule,
    FavoritosPageModule,
    MenuPageModule,
    BuscarPageModule,
    QrScannerPageModule,
    DejarOpinionPageModule,
    EditarPerfilPageModule,
    TabsPageModule,
    MiCuentaPageModule,
    InfoPageModule,
    RestaurantePageModule,
    LoginPageModule,
    RegisterPageModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers: [
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ServiceProvider,
    SplashScreen,
    StatusBar,
    AuthProvider,
    RestaurantesProvider,
    LocationProvider,
    MapaProvider,
    Facebook,
    Geolocation,
    ProfileProvider,
    QRScanner,
    EmailComposer,
    CallNumber,
  ]
})

export class AppModule {}