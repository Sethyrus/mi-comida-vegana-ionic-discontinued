import {
  LocationProvider
} from './../providers/location/location';
import {
  ServiceProvider
} from './../providers/service/service';
import {
  AuthProvider
} from './../providers/auth/auth';
import {
  TabsPage
} from './../pages/tabs/tabs'
import {
  SplashScreen
} from '@ionic-native/splash-screen'
import {
  Component,
  ViewChild
} from '@angular/core'
import {
  StatusBar
} from '@ionic-native/status-bar'
import {
  Platform,
  Events,
  NavController,
  AlertController,
  App
} from 'ionic-angular'
import {
  Storage
} from '@ionic/storage'
import {
  StartPage
} from './../pages/start/start'

@Component({
  templateUrl: 'app.html'
})

export class MyApp {

  @ViewChild('myNav') nav: NavController

  rootPage: any
  alertShown: boolean = false;

  constructor(
    private platform: Platform,
    statusBar: StatusBar,
    private splashScreen: SplashScreen,
    private storage: Storage,
    private authProvider: AuthProvider,
    service: ServiceProvider,
    private location: LocationProvider,
    events: Events,
    private alertCtrl: AlertController,
    app: App,
  ) {
    platform.ready().then(() => {

      events.subscribe('user:logout', () => {
        storage.clear()
        this.nav.push(StartPage)
        this.nav.setRoot(StartPage)
      })

      platform.registerBackButtonAction(() => {
        let activeNav = app.getActiveNav()
        if (!activeNav.canGoBack()) {
          if (this.alertShown === false) {
            this.alertShown = true;
            this.presentConfirm()
          }
        } else {
          activeNav.pop()
        }
      }, 0)

      statusBar.styleDefault()

      storage.get('auth').then((auth) => {
        if (auth === true) {
          storage.get('token').then((token) => {
            if (token) {
              storage.get('pk').then((pk) => {
                if (pk) {
                  service.comprobar_token(token, pk).subscribe(
                    data => {
                      if (data.result == 'ok') {
                        this.authProvider.loadInitialData(() => {
                          this.rootPage = TabsPage
                          this.splashScreen.hide()
                        })
                      } else if (data.result == 'error') {
                        this.goToStartPage()
                      }
                    },
                    error => {
                      this.goToStartPage()
                    }
                  )
                } else {
                  this.goToStartPage()
                }
              })
            } else {
              this.goToStartPage()
            }
          })
        } else {
          this.goToStartPage()
        }
      })
    })
  }

  presentConfirm() {
    let alert = this.alertCtrl.create({
      title: 'Confirmar',
      message: '¿Seguro que quieres salir?',
      buttons: [{
          text: '¡No!',
          role: 'cancel',
          handler: () => {
            this.alertShown = false;
          }
        },
        {
          text: 'Sí',
          handler: () => {
            this.platform.exitApp();
          }
        }
      ]
    })
    alert.present()
  }

  goToStartPage() {
    this.storage.clear()
    this.rootPage = StartPage
    this.splashScreen.hide()
  }
}
