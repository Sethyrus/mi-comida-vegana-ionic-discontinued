import {
  HttpClient
} from '@angular/common/http'
import {
  Injectable
} from '@angular/core'
import {
  ProfileProvider
} from './../../providers/profile/profile';
import {
  LocationProvider
} from './../../providers/location/location';
import {
  ServiceProvider
} from './../../providers/service/service';
import {
  Storage
} from '@ionic/storage';
import {
  RestaurantesProvider
} from '../restaurantes/restaurantes';

@Injectable()

export class AuthProvider {

  constructor(
    public http: HttpClient,
    private service: ServiceProvider,
    private storage: Storage,
    private restaurantes: RestaurantesProvider,
    private location: LocationProvider,
    private profile: ProfileProvider,
  ) { }

  login(email, password, callback) {
    this.service.login(email, password).subscribe(
      data => {
        callback(data)
      },
      error => {
        callback(false)
      }
    )
  }

  setLogin(token, pk, callback) {
    this.storage.set('auth', true).then(() => {
      this.storage.set('token', token).then(() => {
        this.storage.set('pk', pk).then(() => {
          this.loadInitialData(() => {
            callback()
          })
        })
      })
    })
  }

  loadInitialData(callback) {
    this.profile.reloadProfile(() => {
      this.location.reloadCurrentLocation()
      this.restaurantes.reloadRestaurantes(() => {
        callback()
      })
    })
  }

}
