import {
  Injectable
} from '@angular/core';
import {
  Geolocation
} from '@ionic-native/geolocation';
import {
  Events, AlertController
} from 'ionic-angular';

@Injectable()

export class LocationProvider {

  // Ubicación del usuario, obtenida a partir de la ciudad configurada en su perfil.
  // Se utiliza a la hora de cargar el mapa.
  userLocation: any = {}
  // location: any = {}
  // Ubicación física del usuario.
  // Utilizada para determinar la distancia de los restaurantes respecto del usuario.
  currentLocation: any = {}
  isLocationLoaded: boolean = false

  constructor(
    private geolocation: Geolocation,
    private events: Events,
    private alterCtrl: AlertController,
  ) {}

  trackLocation() {
    setTimeout(() => {
      this.reloadCurrentLocation()
    }, 30000)
  }

  reloadCurrentLocation() {
    this.geolocation.getCurrentPosition().then((location) => {
      // let alert = this.alterCtrl.create({
      //   title: 'success',
      //   message: JSON.stringify(location)
      // })
      // alert.present()
      this.currentLocation = location.coords
      console.log('reloadLocation', this.currentLocation)
      this.events.publish('location:loaded')
      this.trackLocation()
    }).catch((e) => {
      let alert = this.alterCtrl.create({
        title: 'error',
        message: JSON.stringify(e)
      })
      alert.present()
      console.log('reloadLocation', this.currentLocation)
      this.currentLocation = JSON.parse(JSON.stringify(this.userLocation))
    })
  }

  // distance(lat, lon) {
  //   return (Math.round(this.getDistanceFromLatLonInKm(lat, lon, this.userLocation.latitude, this.userLocation.longitude) * 10) / 10)
  // }

  // getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
  //   var R = 6371; // Radius of the earth in km
  //   var dLat = this.deg2rad(lat2 - lat1); // deg2rad below
  //   var dLon = this.deg2rad(lon2 - lon1);
  //   var a =
  //     Math.sin(dLat / 2) * Math.sin(dLat / 2) +
  //     Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) *
  //     Math.sin(dLon / 2) * Math.sin(dLon / 2);
  //   var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  //   var d = R * c; // Distance in km
  //   return d;
  // }

  // deg2rad(deg) {
  //   return deg * (Math.PI / 180)
  // }

}
