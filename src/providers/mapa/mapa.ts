import {
  RestaurantesProvider
} from './../restaurantes/restaurantes';
import {
  HttpClient
} from '@angular/common/http';
import {
  Injectable
} from '@angular/core';

@Injectable()

export class MapaProvider {

  markers: any = {}
  isMapLoaded: boolean = false
  isRestaurantsLoaded: boolean = false
  // controla si el mapa está en proceso de carga para controlar otras funcio-
  // nes que dependen de él.
  isMapaLoading: boolean = false

  constructor(
    public http: HttpClient,
    private restaurantes: RestaurantesProvider,
  ) {}

  // loadMap(lat, lng) {
  //   Environment.setEnv({
  //     'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyAmRvB44Oae2Qxf1YH2dhE-zd25P5HrVKg',
  //     'API_KEY_FOR_BROWSER_DEBUG': 'AIzaSyAmRvB44Oae2Qxf1YH2dhE-zd25P5HrVKg'
  //   });
  //   let mapOptions: GoogleMapOptions = {
  //     camera: {
  //       target: {
  //         lat: lat,
  //         lng: lng,
  //       },
  //       zoom: 18,
  //       tilt: 30
  //     }
  //   };
  //   this.map = GoogleMaps.create('map_canvas', mapOptions);
  //   this.isMapLoaded = true
  //   if (this.isRestaurantsLoaded == true) {
  //     this.reloadMapMarkers();
  //   }
  // }

  // reloadMapMarkers() {
  //   if (this.isMapLoaded == true) {
  //     this.restaurantes.restaurantes.forEach((el, i) => {
  //       let marker: Marker = this.map.addMarkerSync({
  //         title: el.nombre,
  //         icon: {
  //           url: 'https://micomidavegana.com/static/media/' + el.imagen,
  //           size: {
  //             width: 40,
  //             height: 40
  //           }
  //         },
  //         animation: 'DROP',
  //         position: {
  //           lat: el.ubicacion_x,
  //           lng: el.ubicacion_y
  //         }
  //       });
  //       this.markers['marker' + i] = marker
  //     });
  //     Object.keys(this.markers).forEach(el => {
  //       this.markers[el].on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
  //         // alert('clicked');
  //       });
  //     });
  //   } else {
  //     this.isRestaurantsLoaded = true
  //   }
  // }

}
