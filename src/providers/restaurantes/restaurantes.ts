import {
  ServiceProvider
} from './../service/service';
import {
  Injectable
} from '@angular/core';
import {
  Events
} from 'ionic-angular';
import {
  Storage
} from '@ionic/storage';
import * as moment from 'moment';
import {
  LocationProvider
} from '../location/location';

@Injectable()

export class RestaurantesProvider {

  restaurantesRaw: any
  restaurantes: any = []
  restaurantesNofree: any = []
  selectedRestaurante: any
  selectedRestauranteCarta: any = {}
  selectedRestauranteProfile: any
  isRestauranteSelected: boolean = false
  favoritos: any = []
  selectedFilter: any = false
  selectedFilters: any = []
  isRestaurantesLoaded: boolean = false
  isMapaLoaded: boolean = false
  selectedMapaRestaurante: any

  constructor(
    private service: ServiceProvider,
    private location: LocationProvider,
    events: Events,
    private storage: Storage,
  ) {
    events.subscribe('location:loaded', () => {
      this.restaurantes.forEach(restaurante => {
        restaurante.distancia = this.distance(restaurante.ubicacion_x, restaurante.ubicacion_y)
      })
    })
  }

  reloadRestaurantes(callback) {
    let restaurantes: any = []
    this.service.get_restaurantes().subscribe(
      data => {
        console.log('get_restaurantes', data)
        if (data.result == 'ok') {
          data.restaurantes.forEach(el => {
            this.sanitizeRestaurantData(el, val => {
              restaurantes.push(val)
            })
          })
          this.restaurantesRaw = restaurantes
          this.filterRestaurantes()
          this.reloadFavoritos()
          this.isRestaurantesLoaded = true
          // this.events.publish('restaurants:reloaded')
          callback()
        }
      },
      error => {}
    )
  }

  filterRestaurantes() {
    let restaurantesRaw = JSON.parse(JSON.stringify(this.restaurantesRaw))
    this.restaurantes = []
    if (this.selectedFilters.length > 0) {
      restaurantesRaw.forEach(el => {
        this.selectedFilters.forEach(subEl => {
          if (el.tipo == subEl) {
            this.restaurantes.push(el)
          }
        });
      })
    } else {
      this.restaurantes = restaurantesRaw
    }
    this.sortRestaurantes()
    this.filterRestaurantesNofree()
    console.log('this.restaurantes', this.restaurantes)
    console.log('this.restaurantesNofree', this.restaurantesNofree)
  }

  sortRestaurantes() {
    let restaurantesSorted = []
    let restaurantes = []
    let restaurantesRaw = JSON.parse(JSON.stringify(this.restaurantes))

    restaurantesSorted = restaurantesRaw.sort(function (a, b) {
      return a.distancia - b.distancia
    })

    let pos: number = 0

    for (pos = 0; pos < restaurantesSorted.length; pos++) {
      if (restaurantesSorted[pos].abierto) {
        restaurantes.push(restaurantesSorted[pos])
        restaurantesSorted.splice(pos, 1)
        pos--
      }
    }

    restaurantes.push(...restaurantesSorted)
    this.restaurantes = restaurantes
  }

  filterRestaurantesNofree() {
    this.restaurantesNofree = []
    this.restaurantes.forEach(el => {
      if (el.gratis == "False") {
        this.restaurantesNofree.push(el)
      }
    })
  }

  reloadFavoritos() {
    this.storage.get('token').then((token) => {
      this.storage.get('pk').then((pk) => {
        this.service.get_favoritos(pk, token).subscribe(
          data => {
            let favoritos = []
            if (data.restaurantes_favoritos) {
              let favoritosRaw = data.restaurantes_favoritos
              this.restaurantes.forEach(el => {
                favoritosRaw.forEach(subEl => {
                  if (el.id == subEl) {
                    favoritos.push(el)
                    el.favorite = true
                  } else {
                    el.favorite = false
                  }
                })
              })
            }
            this.favoritos = favoritos
          },
          error => {
          }
        )
      })
    })
  }

  sanitizeRestaurantData(data, callback) {
    let horario = this.generateHorario(data)
    let sanitizedData: any = {
      id: data.pk,
      nombre: data.nombre,
      ciudad: data.ciudad.nombre,
      descripcion_corta: data.descripcion_corta,
      direccion: data.direccion,
      imagen: data.foto_perfil,
      ubicacion_x: data.ubicacion_x,
      ubicacion_y: data.ubicacion_y,
      distancia: this.distance(data.ubicacion_x, data.ubicacion_y),
      gratis: data.gratis,
      tipo: data.comida,
      abierto: this.isRestauranteOpen(horario),
      horario: horario,
      slug: this.genSlug(data.nombre)
    }
    callback(sanitizedData)
  }

  generateHorario(data) {
    return {
      lunes: {
        mañana: {
          h: data.lunes_a,
          t: data.lunes_tiempo_a,
        },
        tarde: {
          h: data.lunes_tarde_a,
          t: data.lunes_tarde_tiempo_a,
        },
      },
      martes: {
        mañana: {
          h: data.martes_a,
          t: data.martes_tiempo_a,
        },
        tarde: {
          h: data.martes_tarde_a,
          t: data.martes_tarde_tiempo_a,
        },
      },
      miercoles: {
        mañana: {
          h: data.miercoles_a,
          t: data.miercoles_tiempo_a,
        },
        tarde: {
          h: data.miercoles_tarde_a,
          t: data.miercoles_tarde_tiempo_a,
        },
      },
      jueves: {
        mañana: {
          h: data.jueves_a,
          t: data.jueves_tiempo_a,
        },
        tarde: {
          h: data.jueves_tarde_a,
          t: data.jueves_tarde_tiempo_a,
        },
      },
      viernes: {
        mañana: {
          h: data.viernes_a,
          t: data.viernes_tiempo_a,
        },
        tarde: {
          h: data.viernes_tarde_a,
          t: data.viernes_tarde_tiempo_a,
        },
      },
      sabado: {
        mañana: {
          h: data.sabado_a,
          t: data.sabado_tiempo_a,
        },
        tarde: {
          h: data.sabado_tarde_a,
          t: data.sabado_tarde_tiempo_a,
        },
      },
      domingo: {
        mañana: {
          h: data.domingo_a,
          t: data.domingo_tiempo_a,
        },
        tarde: {
          h: data.domingo_tarde_a,
          t: data.domingo_tarde_tiempo_a,
        },
      },
    }
  }

  genSlug(name) {
    name = name.toLowerCase()
    name = name.normalize('NFD').replace(/[\u0300-\u036f]/g, "")
    name = name.replace(new RegExp(' ', 'g'), '-');
    return name
  }

  isRestauranteOpen(horario) {
    let format = 'hh:mm:ss'
    let isOpen: boolean = false

    switch (moment().day()) {
      case 0:
        {
          let before1 = moment(horario['domingo'].mañana.h, format, 'h:mm:ss')
          let after1 = moment(horario['domingo'].mañana.h, format, 'h:mm:ss').add(horario['domingo'].mañana.t, 'hours')
          let before2 = moment(horario['domingo'].tarde.h, format, 'h:mm:ss')
          let after2 = moment(horario['domingo'].tarde.h, format, 'h:mm:ss').add(horario['domingo'].tarde.t, 'hours')
          if (moment().isBetween(before1, after1) || moment().isBetween(before2, after2)) {
            isOpen = true
          }
          return isOpen
        }
      case 1:
        {
          let before1 = moment(horario['lunes'].mañana.h, format, 'h:mm:ss')
          let after1 = moment(horario['lunes'].mañana.h, format, 'h:mm:ss').add(horario['lunes'].mañana.t, 'hours')
          let before2 = moment(horario['lunes'].tarde.h, format, 'h:mm:ss')
          let after2 = moment(horario['lunes'].tarde.h, format, 'h:mm:ss').add(horario['lunes'].tarde.t, 'hours')
          if (moment().isBetween(before1, after1) || moment().isBetween(before2, after2)) {
            isOpen = true
          }
          return isOpen
        }
      case 2:
        {
          let before1 = moment(horario['martes'].mañana.h, format, 'h:mm:ss')
          let after1 = moment(horario['martes'].mañana.h, format, 'h:mm:ss').add(horario['martes'].mañana.t, 'hours')
          let before2 = moment(horario['martes'].tarde.h, format, 'h:mm:ss')
          let after2 = moment(horario['martes'].tarde.h, format, 'h:mm:ss').add(horario['martes'].tarde.t, 'hours')
          if (moment().isBetween(before1, after1) || moment().isBetween(before2, after2)) {
            isOpen = true
          }

          return isOpen
        }
      case 3:
        {
          let before1 = moment(horario['miercoles'].mañana.h, format, 'h:mm:ss')
          let after1 = moment(horario['miercoles'].mañana.h, format, 'h:mm:ss').add(horario['miercoles'].mañana.t, 'hours')
          let before2 = moment(horario['miercoles'].tarde.h, format, 'h:mm:ss')
          let after2 = moment(horario['miercoles'].tarde.h, format, 'h:mm:ss').add(horario['miercoles'].tarde.t, 'hours')
          if (moment().isBetween(before1, after1) || moment().isBetween(before2, after2)) {
            isOpen = true
          }

          return isOpen
        }
      case 4:
        {
          let before1 = moment(horario['jueves'].mañana.h, format, 'h:mm:ss')
          let after1 = moment(horario['jueves'].mañana.h, format, 'h:mm:ss').add(horario['jueves'].mañana.t, 'hours')
          let before2 = moment(horario['jueves'].tarde.h, format, 'h:mm:ss')
          let after2 = moment(horario['jueves'].tarde.h, format, 'h:mm:ss').add(horario['jueves'].tarde.t, 'hours')
          if (moment().isBetween(before1, after1) || moment().isBetween(before2, after2)) {
            isOpen = true
          }

          return isOpen
        }
      case 5:
        {
          let before1 = moment(horario['viernes'].mañana.h, 'h:mm:ss')
          let after1 = moment(horario['viernes'].mañana.h, 'h:mm:ss').add(horario['viernes'].mañana.t, 'hours')
          let before2 = moment(horario['viernes'].tarde.h, 'h:mm:ss')
          let after2 = moment(horario['viernes'].tarde.h, 'h:mm:ss').add(horario['viernes'].tarde.t, 'hours')
          if (moment().isBetween(before1, after1) || moment().isBetween(before2, after2)) {
            isOpen = true
          }

          return isOpen
        }
      case 6:
        {
          let before1 = moment(horario['sabado'].mañana.h, 'h:mm:ss')
          let after1 = moment(horario['sabado'].mañana.h, 'h:mm:ss').add(horario['sabado'].mañana.t, 'hours')
          let before2 = moment(horario['sabado'].tarde.h, 'h:mm:ss')
          let after2 = moment(horario['sabado'].tarde.h, 'h:mm:ss').add(horario['sabado'].tarde.t, 'hours')
          if (moment().isBetween(before1, after1) || moment().isBetween(before2, after2)) {
            isOpen = true
          }

          return isOpen
        }
    }
  }

  distance(lat, lon) {
    return (Math.round(this.getDistanceFromLatLonInKm(lat, lon, this.location.currentLocation.latitude, this.location.currentLocation.longitude) * 10) / 10)
  }

  getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = this.deg2rad(lat2 - lat1); // deg2rad below
    var dLon = this.deg2rad(lon2 - lon1);
    var a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) *
      Math.sin(dLon / 2) * Math.sin(dLon / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c; // Distance in km
    return d;
  }

  deg2rad(deg) {
    return deg * (Math.PI / 180)
  }

}
