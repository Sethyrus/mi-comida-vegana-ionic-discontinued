import { LocationProvider } from './../location/location';
import { Storage } from '@ionic/storage';
import { ServiceProvider } from './../service/service';
import { Injectable } from '@angular/core';

@Injectable()

export class ProfileProvider {

  username: string
  email: string
  ciudad: string
  publicidad: boolean
  tipo: string

  constructor(
    private service: ServiceProvider,
    private storage: Storage,
    private location: LocationProvider,
  ) {}

  get locationByCity() {
    switch(this.ciudad) {
      default: {
        return {
          latitude: 37.1773311,
          longitude: -3.6335766,
        }
      }
      case('San Sebastián'): {
        return {
          latitude: 43.3224219,
          longitude: -1.9838889,
        }
      }
      case('Zaragoza'): {
        return {
          latitude: 41.63379739999047,
          longitude: -0.8907796013221514,
        }
      }
      case('Zamora'): {
        return {
          latitude: 41.5056574,
          longitude: -5.7448039,
        }
      }
      case('Vizcaya'): {
        return {
          latitude: 43.2181939,
          longitude: -3.4913674,
        }
      }
      case('Valladolid'): {
        return {
          latitude: 41.6521328,
          longitude: -4.728562,
        }
      }
      case('Toledo'): {
        return {
          latitude: 39.8560679,
          longitude: -4.0239568,
        }
      }
      case('Teruel'): {
        return {
          latitude: 40.3436719,
          longitude: -1.1081939,
        }
      }
      case('Tarragona'): {
        return {
          latitude: 41.1172364,
          longitude: 1.2546057,
        }
      }
      case('Soria'): {
        return {
          latitude: 41.7633842,
          longitude: -2.4642041,
        }
      }
      case('Sevilla'): {
        return {
          latitude: 37.3886303,
          longitude: -5.9953171,
        }
      }
      case('Segovia'): {
        return {
          latitude: 40.9502159,
          longitude: -4.1241494,
        }
      }
      case('El Hierro'): {
        return {
          latitude: 27.7254949,
          longitude: -18.0593206,
        }
      }
      case('Las Palmas'): {
        return {
          latitude: 28.128874,
          longitude: -15.4349448,
        }
      }
      case('La Gomera'): {
        return {
          latitude: 28.1032991,
          longitude: -17.2543774,
        }
      }
      case('Tenerife'): {
        return {
          latitude: 28.4636251,
          longitude: -16.2868663,
        }
      }
      case('Salamanca'): {
        return {
          latitude: 40.9651572,
          longitude: -5.6640182,
        }
      }
      case('Pontevedra'): {
        return {
          latitude: 42.3618865,
          longitude: -8.8555432,
        }
      }
      case('Palencia'): {
        return {
          latitude: 42.0108856,
          longitude: -4.5328735,
        }
      }
      case('Orense'): {
        return {
          latitude: 42.335768,
          longitude: -7.933921,
        }
      }
      case('Navarra'): {
        return {
          latitude: 42.6125488,
          longitude: -1.8307877,
        }
      }
      case('Murcia'): {
        return {
          latitude: 37.9923795,
          longitude: -1.1305431,
        }
      }
      case('Menorca'): {
        return {
          latitude: 39.9496234,
          longitude: 4.0754254,
        }
      }
      case('Mallorca'): {
        return {
          latitude: 39.6952576,
          longitude: 2.9825517,
        }
      }
      case('Lugo'): {
        return {
          latitude: 43.009717,
          longitude: -7.6267981,
        }
      }
      case('Lérida'): {
        return {
          latitude: 41.6147605,
          longitude: 0.6267842,
        }
      }
      case('León'): {
        return {
          latitude: 42.5989995,
          longitude: -5.5682413,
        }
      }
      case('Lanzarote'): {
        return {
          latitude: 29.046849,
          longitude: -13.6249929,
        }
      }
      case('Fuerteventura'): {
        return {
          latitude: 28.3587391,
          longitude: -14.0886956,
        }
      }
      case('Gran Canaria'): {
        return {
          latitude: 27.9202158,
          longitude: -15.5824569,
        }
      }
      case('La Rioja'): {
        return {
          latitude: 42.3285528,
          longitude: -2.4674917,
        }
      }
      case('Jaén'): {
        return {
          latitude: 37.7728858,
          longitude: -3.7883289,
        }
      }
      case('Ibiza'): {
        return {
          latitude: 39.0200047,
          longitude: 1.4471287,
        }
      }
      case('Aragón'): {
        return {
          latitude: 42.1360614,
          longitude: -0.0298027,
        }
      }
      case('Huelva'): {
        return {
          latitude: 37.2575874,
          longitude: -6.9484945,
        }
      }
      case('Guipúzcoa'): {
        return {
          latitude: 43.0756085,
          longitude: -2.2937066,
        }
      }
      case('Guadalajara'): {
        return {
          latitude: 40.6324837,
          longitude: -3.1951895,
        }
      }
      case('Girona'): {
        return {
          latitude: 41.9793792,
          longitude: 2.7513864,
        }
      }
      case('Formentera'): {
        return {
          latitude: 38.6963954,
          longitude: 1.418116,
        }
      }
      case('Cuenca'): {
        return {
          latitude: 40.0703872,
          longitude: -2.1724357,
        }
      }
      case('Córdoba'): {
        return {
          latitude: 37.8881699,
          longitude: -4.814403,
        }
      }
      case('Ciudad Real'): {
        return {
          latitude: 38.9848243,
          longitude: -3.9623973,
        }
      }
      case('Ceuta'): {
        return {
          latitude: 35.8893823,
          longitude: -5.356365,
        }
      }
      case('Castellón'): {
        return {
          latitude: 39.986351,
          longitude: -0.0863441,
        }
      }
      case('Cantrabria'): {
        return {
          latitude: 43.1828182,
          longitude: -4.0578826,
        }
      }
      case('Cádiz'): {
        return {
          latitude: 36.5270561,
          longitude: -6.3236157,
        }
      }
      case('Cáceres'): {
        return {
          latitude: 39.4752712,
          longitude: -6.4074442,
        }
      }
      case('Burgos'): {
        return {
          latitude: 42.3439712,
          longitude: -3.766946,
        }
      }
      case('Badajoz'): {
        return {
          latitude: 38.8794443,
          longitude: -7.005673,
        }
      }
      case('Ávila'): {
        return {
          latitude: 40.6566797,
          longitude: -4.7162281,
        }
      }
      case('Asturias'): {
        return {
          latitude: 43.3613739,
          longitude: -5.9293666,
        }
      }
      case('Almería'): {
        return {
          latitude: 36.8340419,
          longitude: -2.4987331,
        }
      }
      case('Alicante'): {
        return {
          latitude: 38.3459911,
          longitude: -0.525705,
        }
      }
      case('Albacete'): {
        return {
          latitude: 38.9943438,
          longitude: -1.8935619,
        }
      }
      case('Álava'): {
        return {
          latitude: 42.9099776,
          longitude: -2.7684268,
        }
      }
      case('A Coruña'): {
        return {
          latitude: 43.3623222,
          longitude: -8.48158,
        }
      }
      case('Málaga'): {
        return {
          latitude: 36.7212686,
          longitude: -4.4564183,
        }
      }
      case('Bilbao'): {
        return {
          latitude: 43.2629912,
          longitude: -3.0050251,
        }
      }
      case('Valencia'): {
        return {
          latitude: 39.4699022,
          longitude: -0.4113076,
        }
      }
      case('Barcelona'): {
        return {
          latitude: 41.3850427,
          longitude: 2.1033635,
        }
      }
      case('Granada'): {
        return {
          latitude: 37.1773311,
          longitude: -3.6335766,
        }
      }
      case('Madrid'): {
        return {
          latitude: 40.4167701,
          longitude: -3.7388097,
        }
      }
    }
  }

  reloadProfile(callback) {
    this.storage.get('token').then((token) => {
      this.storage.get('pk').then((pk) => {
        this.service.get_perfil(pk, token).subscribe(
          data => {
            this.username = data.username
            this.email = data.email
            this.ciudad = data.ciudad
            this.publicidad = data.publicidad
            this.tipo = data.tipo
            this.location.userLocation = this.locationByCity
            console.log('this.location.userLocation', this.location.userLocation)
            callback()
          },
          error => {
          }
        )
      })
    })
  }
}
