import {
  HttpClient,
  HttpHeaders
} from '@angular/common/http';
import {
  Injectable
} from '@angular/core';
import {
  Observable
} from 'rxjs/Observable';


@Injectable()

export class ServiceProvider {

  private userUrl: string =         'https://micomidavegana.com/usuarios/ionic/'
  private restaurantesUrl: string = 'https://micomidavegana.com/restaurantes/ionic/'

  constructor(
    public http: HttpClient
  ) {}

  register(usuario, email, password, ciudad, publicidad): Observable < any > {
    let headers = new HttpHeaders()
    let formData = new FormData();
    let data = {
      usuario: usuario,
      email: email,
      password: password,
      ciudad: ciudad,
      publicidad: publicidad
    }

    formData.append("data", JSON.stringify(data))

    return this.http.post(this.userUrl + 'registrar_usuario/', formData, {
      headers: headers
    });
  }

  login(usuario, password): Observable < any > {
    let headers = new HttpHeaders()
    let formData = new FormData();
    let data = {
      email: usuario,
      password: password,
    }

    formData.append("data", JSON.stringify(data))

    return this.http.post(this.userUrl + 'login/', formData, {
      headers: headers
    });
  }

  loginSocial(email, nombre): Observable < any > {
    let headers = new HttpHeaders()
    let formData = new FormData();
    let data = {
      email: email,
      nombre: nombre,
    }

    formData.append("data", JSON.stringify(data))
    return this.http.post(this.userUrl + 'registrar_usuario_facebook_google/', formData, {
      headers: headers
    });
  }

  logout(usuario_id, token): Observable < any > {
    let headers = new HttpHeaders()
    let formData = new FormData();
    let data = {
      usuario_id: usuario_id,
      token: token,
    }

    formData.append("data", JSON.stringify(data))

    return this.http.post(this.userUrl + 'logout/', formData, {
      headers: headers
    });
  }

  comprobar_token(token, id): Observable < any > {
    let headers = new HttpHeaders()
    let formData = new FormData();
    let data = {
      token: token,
      usuario_id: id,
    }

    formData.append("data", JSON.stringify(data))

    return this.http.post(this.userUrl + 'comprobar_token/', formData, {
      headers: headers
    });
  }

  get_restaurantes(): Observable < any > {
    let headers = new HttpHeaders()

    return this.http.get(this.restaurantesUrl + 'get_restaurantes/', {
      headers: headers
    });
  }

  get_ciudades(): Observable < any > {
    let headers = new HttpHeaders()

    return this.http.get(this.restaurantesUrl + 'get_ciudades/', {
      headers: headers
    });
  }

  get_perfil_restaurante(pk): Observable < any > {
    let headers = new HttpHeaders()
    let formData = new FormData();
    let data = {
      restaurante_id: pk,
    }

    formData.append("data", JSON.stringify(data))

    return this.http.post(this.restaurantesUrl + 'get_perfil_restaurantes/', formData, {
      headers: headers
    });
  }

  get_perfil(usuario_id, token): Observable < any > {
    let headers = new HttpHeaders()
    let formData = new FormData();
    let data = {
      usuario_id: usuario_id,
      token: token,
    }

    formData.append("data", JSON.stringify(data))

    return this.http.post(this.userUrl + 'get_perfil/', formData, {
      headers: headers
    });
  }

  get_favoritos(usuario_id, token): Observable < any > {
    let headers = new HttpHeaders()
    let formData = new FormData();
    let data = {
      usuario_id: usuario_id,
      token: token,
    }

    formData.append("data", JSON.stringify(data))

    return this.http.post(this.restaurantesUrl + 'get_favoritos/', formData, {
      headers: headers
    });
  }

  registrar_favorito(usuario_id, restaurante_id, token): Observable < any > {
    let headers = new HttpHeaders()
    let formData = new FormData();
    let data = {
      usuario_id: usuario_id,
      restaurante_pk: restaurante_id,
      token: token,
    }

    formData.append("data", JSON.stringify(data))

    return this.http.post(this.restaurantesUrl + 'registrar_favorito/', formData, {
      headers: headers
    });
  }

  comprobar_valoracion_abierta(usuario_id, restaurante_id, token): Observable < any > {
    let headers = new HttpHeaders()
    let formData = new FormData();
    let data = {
      usuario_id: usuario_id,
      restaurante_pk: restaurante_id,
      token: token,
    }

    formData.append("data", JSON.stringify(data))

    return this.http.post(this.restaurantesUrl + 'comprobar_valoracion_abierta/', formData, {
      headers: headers
    });
  }

  comprobar_codigoqr(usuario_id, restaurante_id, token, codigo_qr): Observable < any > {
    let headers = new HttpHeaders()
    let formData = new FormData();
    let data = {
      usuario_id: usuario_id,
      restaurante_pk: restaurante_id,
      token: token,
      codigo_qr: codigo_qr,
    }

    formData.append("data", JSON.stringify(data))

    return this.http.post(this.restaurantesUrl + 'comprobar_codigoqr/', formData, {
      headers: headers
    });
  }

  registrar_valoracion(usuario_id, restaurante_id, token, calidad, espera, comida, texto): Observable < any > {
    let headers = new HttpHeaders()
    let formData = new FormData();
    let data = {
      usuario_id: usuario_id,
      restaurante_pk: restaurante_id,
      token: token,
      calidad: calidad,
      espera: espera,
      comida: comida,
      texto: texto,
    }

    formData.append("data", JSON.stringify(data))

    return this.http.post(this.restaurantesUrl + 'registrar_favorito/', formData, {
      headers: headers
    });
  }

  cambiar_pass(usuario_id, token, antigua, nueva): Observable < any > {
    let headers = new HttpHeaders()
    let formData = new FormData();
    let data = {
      usuario_id: usuario_id,
      token: token,
      antigua: antigua,
      nueva: nueva
    }

    formData.append("data", JSON.stringify(data))

    return this.http.post(this.userUrl + 'cambiar_pass/', formData, {
      headers: headers
    });
  }

  cambiar_datos(usuario_id, token, datos): Observable < any > {
    let headers = new HttpHeaders()
    let formData = new FormData();
    let data = {
      usuario_id: usuario_id,
      token: token,
      ...datos
    }

    formData.append("data", JSON.stringify(data))

    return this.http.post(this.userUrl + 'cambiar_datos/', formData, {
      headers: headers
    });
  }
}
