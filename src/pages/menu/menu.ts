import {
  StatusBar
} from '@ionic-native/status-bar';
import {
  InfoPage
} from './../info/info';
import {
  MiCuentaPage
} from './../mi-cuenta/mi-cuenta';
import {
  Component,
  ViewChild
} from '@angular/core';
import {
  IonicPage,
  NavController,
  Events,
  Tabs
} from 'ionic-angular';
import {
  RestaurantesProvider
} from '../../providers/restaurantes/restaurantes';

@IonicPage()

@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})

export class MenuPage {

  @ViewChild('tabs') tabs: Tabs;
  isAplyingfilter: boolean = false

  constructor(
    public navCtrl: NavController,
    private statusBar: StatusBar,
    public restaurantes: RestaurantesProvider,
    private events: Events,
  ) {}

  toggleFilter(filter: string) {
    if (this.restaurantes.selectedFilters.includes(filter)) {
      let pos = this.restaurantes.selectedFilters.indexOf(filter)
      this.restaurantes.selectedFilters.splice(pos, 1)
    } else {
      this.restaurantes.selectedFilters.push(filter)
    }
    this.restaurantes.filterRestaurantes()
  }

  isFilterActive(filter: string) {
    if (this.restaurantes.selectedFilters.includes(filter)) {
      return true
    } else {
      return false
    }
  }

  ionViewWillEnter() {
    this.statusBar.backgroundColorByHexString('#e9e9e9')
  }

  goToMapa() {
    this.events.publish('tabs:switch', 0)
  }

  goToLista() {
    this.events.publish('tabs:switch', 1)
  }

  goToMiCuenta() {
    this.navCtrl.push(MiCuentaPage)
  }

  goToInfo() {
    this.navCtrl.push(InfoPage)
  }

}
