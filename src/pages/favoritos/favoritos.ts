import { LocationProvider } from './../../providers/location/location';
import { StatusBar } from '@ionic-native/status-bar';
import { RestaurantePage } from './../restaurante/restaurante';
import { ServiceProvider } from './../../providers/service/service';
import { InfoPage } from './../info/info';
import { MiCuentaPage } from './../mi-cuenta/mi-cuenta';
import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController } from 'ionic-angular';
import { RestaurantesProvider } from '../../providers/restaurantes/restaurantes';
import * as moment from 'moment';

@IonicPage()

@Component({
  selector: 'page-favoritos',
  templateUrl: 'favoritos.html',
})

export class FavoritosPage {

  constructor(
    public navCtrl: NavController,
    public restaurantes: RestaurantesProvider,
    private service: ServiceProvider,
    private alertCtrl: AlertController,
    private statusBar: StatusBar,
    private location: LocationProvider,
  ) {}

  goToMiCuenta() {
    this.navCtrl.push(MiCuentaPage)
  }

  goToInfo() {
    this.navCtrl.push(InfoPage)
  }

  ionViewWillEnter() {
    this.statusBar.backgroundColorByHexString('#e9e9e9')
  }

  goToThisRestaurante(restaurante) {
    this.service.get_perfil_restaurante(restaurante.id).subscribe(
      data => {
        if (data.result == 'ok') {
          this.restaurantes.selectedRestaurante = restaurante
          this.restaurantes.selectedRestauranteProfile = data.perfil_restaurantes[0]
          this.restaurantes.isRestauranteSelected = true
          this.navCtrl.push(RestaurantePage)
        } else {
          let alert = this.alertCtrl.create({
            title: "Error",
            message: "Ha ocurrido un error al cargar el restaurante. Inténtalo de nuevo más tarde.",
            buttons: [
              {
                text: "OK",
                role: "cancel"
              }
            ]
          })
          alert.present()
        }
      },
      error => {
        let alert = this.alertCtrl.create({
          title: "Error",
          message: "Ha ocurrido un error al cargar el restaurante. Inténtalo de nuevo más tarde.",
          buttons: [
            {
              text: "OK",
              role: "cancel"
            }
          ]
        })
        alert.present()
      }
    )
  }
}