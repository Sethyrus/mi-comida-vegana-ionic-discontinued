import {
  StartPage
} from './../start/start';
import {
  ServiceProvider
} from './../../providers/service/service';
import {
  ProfileProvider
} from './../../providers/profile/profile';
import {
  Component
} from '@angular/core';
import {
  IonicPage,
  NavController,
  AlertController,
  Events
} from 'ionic-angular';
import {
  Storage
} from '@ionic/storage';
import {
  StatusBar
} from '@ionic-native/status-bar';
import {
  EditarPerfilPage
} from '../editar-perfil/editar-perfil';
import { EmailComposer } from '@ionic-native/email-composer';

@IonicPage()

@Component({
  selector: 'page-mi-cuenta',
  templateUrl: 'mi-cuenta.html',
})

export class MiCuentaPage {

  constructor(
    public navCtrl: NavController,
    public profile: ProfileProvider,
    private alertController: AlertController,
    private service: ServiceProvider,
    private storage: Storage,
    private events: Events,
    private statusBar: StatusBar,
    private email: EmailComposer,
  ) {}

  goBack() {
    this.navCtrl.pop()
  }

  ionViewWillEnter() {
    this.statusBar.backgroundColorByHexString('#e9e9e9')
  }

  goToEditarPerfil() {
    this.navCtrl.push(EditarPerfilPage)
  }

  tryLogout() {
    let alert = this.alertController.create({
      title: "¿Seguro que quieres cerrar sesión?",
      buttons: [{
          text: "Cancelar",
          role: "cancel"
        },
        {
          text: "Aceptar",
          handler: () => {

            this.storage.get('token').then((token) => {
              this.storage.get('pk').then((pk) => {
                this.service.logout(pk, token).subscribe(
                  data => {
                    this.events.publish('user:logout')
                  },
                  error => {
                  }
                )
              })
            })
          }
        }
      ]
    })
    alert.present()
  }
  
  trySendEmail() {
    // this.email.isAvailable().then((available: boolean) => {
    //   let alert = this.alertController.create({
    //     message: available.toString()
    //   })
    //   alert.present()
    //   if (available) {
        let email = {
          to: 'info@micomidavegana.com',
        };
        this.email.open(email);
    //   }
    //  });
  }
}
