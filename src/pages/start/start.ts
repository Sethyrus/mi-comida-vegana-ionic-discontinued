import { TabsPage } from './../tabs/tabs';
import { AuthProvider } from './../../providers/auth/auth';
import { ServiceProvider } from './../../providers/service/service';
import {
  LoginPage
} from './../login/login';
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController
} from 'ionic-angular';
import {
  Component
} from '@angular/core';
import {
  StatusBar
} from '@ionic-native/status-bar';
import {
  RegisterPage
} from '../register/register';
import {
  Facebook,
  FacebookLoginResponse
} from '@ionic-native/facebook';
import { PrimeraVezPage } from '../primera-vez/primera-vez';

@IonicPage()

@Component({
  selector: 'page-start',
  templateUrl: 'start.html'
})

export class StartPage {

  constructor(
    public navParams: NavParams,
    public navCtrl: NavController,
    private statusBar: StatusBar,
    private alertCtrl: AlertController,
    private facebook: Facebook,
    private service: ServiceProvider,
    private authProvider: AuthProvider,
  ) {}

  ionViewWillEnter() {
    this.statusBar.backgroundColorByHexString('#fbae17')
  }

  goToLogin() {
    this.navCtrl.push(LoginPage)
  }

  goToRegister() {
    this.navCtrl.push(RegisterPage)
  }

  goToPrimeraVez() {
    this.navCtrl.push(PrimeraVezPage)
    this.navCtrl.setRoot(PrimeraVezPage)
  }

  loginFacebook() {this.facebook.login(['email', 'public_profile'])
  .then((res: FacebookLoginResponse) => {
    this.facebook.api('me?fields=id,name,email,first_name,picture.width(720).height(720).as(picture_large)', []).then(profile => {
      this.service.loginSocial(profile.email, profile.name).subscribe(
        data => {
          if (data.result == 'ok') {
            if (data.primera_vez) {
              this.authProvider.setLogin(data.token, data.pk, () => {
                this.navCtrl.push(PrimeraVezPage)
                this.navCtrl.setRoot(PrimeraVezPage)
              })
            } else {
              this.authProvider.setLogin(data.token, data.pk, () => {
                this.navCtrl.push(TabsPage)
                this.navCtrl.setRoot(TabsPage)
              })
            }
          } else {
            let alert = this.alertCtrl.create({
              title: 'Error',
              // message: JSON.stringify(data),
              message: 'Ha habido un error al iniciar sesión',
            })
            alert.present()
          }
        },
        error => {
          let alert = this.alertCtrl.create({
            title: 'Error',
            // message: JSON.stringify(error),
              message: 'Ha habido un error al iniciar sesión. ¿Es posible que ya te hayas registrado con una contraseña?',
          })
          alert.present()
        }
      )
    }).catch((e) => {
      let alert = this.alertCtrl.create({
        title: 'Error',
        // message: JSON.stringify(e),
        message: 'Ha habido un error obteniendo los datos de tu perfil'
      })
      alert.present()
    })
  })
  .catch(e => {
    let alert = this.alertCtrl.create({
      title: 'Error',
      // message: JSON.stringify(e),
      message: 'Ha habido un error al iniciar sesión a través de Facebook'
    })
    alert.present()
  });
  }

}
