import {
  AuthProvider
} from './../../providers/auth/auth';
import {
  TabsPage
} from './../../pages/tabs/tabs';
import {
  FormBuilder,
  Validators
} from '@angular/forms';
import {
  Component
} from '@angular/core';
import {
  NavController,
  AlertController,
  IonicPage
} from 'ionic-angular';

@IonicPage()

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})

export class LoginPage {

  loginForm: any = this.fb.group({
    email: [''],
    password: ['', Validators.minLength(3)],
  });

  constructor(
    private fb: FormBuilder,
    private navCtrl: NavController,
    private authProvider: AuthProvider,
    private alertCtrl: AlertController,
  ) { }
  
  volver() {
    this.navCtrl.pop()
  }

  login() {
    let email = this.loginForm.controls.email.value
    let password = this.loginForm.controls.password.value
    if (email.length > 0) {
      if (password.length > 0) {
        this.authProvider.login(email, password, (data) => {
          if (data) {
            if (data.result == 'ok') {
              this.authProvider.setLogin(data.token, data.pk, () => {
                this.navCtrl.push(TabsPage)
                this.navCtrl.setRoot(TabsPage)
              })
            } else if (data.result == 'error') {
              let alert = this.alertCtrl.create({
                title: 'Error',
                message: "Los datos introducidos son incorrectos",
                buttons: [{
                  text: 'Aceptar',
                  role: 'cancel'
                }]
              })
              alert.present()
            } else {
              let alert = this.alertCtrl.create({
                title: 'Error',
                message: 'Ha habido un error al iniciar sesión. Si el problema se repite, contáctanos.',
                buttons: [{
                  text: 'Aceptar',
                  role: 'cancel'
                }]
              })
              alert.present()
            }
          } else {
            let alert = this.alertCtrl.create({
              title: 'Error',
              message: 'Ha ocurrido un error. Vuelve a intentarlo más tarde.',
              buttons: [{
                text: 'Aceptar',
                role: 'cancel'
              }]
            })
            alert.present()
          }
        })
      } else {
        let alert = this.alertCtrl.create({
          title: 'Error',
          message: 'La contraseña no puede estar en blanco.',
          buttons: [{
            text: 'Aceptar',
            role: 'cancel'
          }]
        })
        alert.present()
      }
    } else {
      let alert = this.alertCtrl.create({
        title: 'Error',
        message: 'El correo electrónico no puede estar en blanco.',
        buttons: [{
          text: 'Aceptar',
          role: 'cancel'
        }]
      })
      alert.present()
    }
  }
}