import {
  LocationProvider
} from './../../providers/location/location';
import {
  StatusBar
} from '@ionic-native/status-bar';
import {
  ServiceProvider
} from './../../providers/service/service';
import {
  RestaurantesProvider
} from './../../providers/restaurantes/restaurantes';
import {
  MiCuentaPage
} from './../mi-cuenta/mi-cuenta';
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController,
  AlertOptions
} from 'ionic-angular';
import {
  Component,
  ɵConsole
} from '@angular/core';
import {
  InfoPage
} from '../info/info';
import {
  RestaurantePage
} from '../restaurante/restaurante';
import {
  Storage
} from '@ionic/storage';
import {
  FormGroup,
  FormBuilder,
  Validators
} from '@angular/forms';
import * as moment from 'moment';

@IonicPage()

@Component({
  selector: 'page-buscar',
  templateUrl: 'buscar.html',
})

export class BuscarPage {

  showSearch: boolean
  searchList: any
  recientesList: any = []
  searchForm: any = this.FormBuilder.group({
    restaurantName: ['', Validators.required],
    restaurantCity: [''],
    filterVegana: [true],
    filterVegetariana: [true],
    filterOpcion: [true],
    filterTienda: [true],
  })
  listaCiudades: any

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private restaurantes: RestaurantesProvider,
    private service: ServiceProvider,
    private alertCtrl: AlertController,
    private storage: Storage,
    private statusBar: StatusBar,
    private location: LocationProvider,
    private FormBuilder: FormBuilder,
  ) {}

  // resetCity() {
  //   this.searchForm["controls"]["restaurantCity"].reset()
  // }

  ionViewDidLoad() {
    this.storage.get('recientesList').then(val => {
      if (val && val.length > 0) {
        this.recientesList = JSON.parse(val)
      }
    })
    if (!this.listaCiudades) {
      this.service.get_ciudades().subscribe(
        data => {
          let ciudades = []
          data.Ciudades.forEach(el => {
            ciudades.push({
              nombre: el.nombre,
              pk: el.pk,
              slug: this.genSlug(el.nombre)
            })
          })
          ciudades = ciudades.sort(function (a, b) {
            if (a.slug < b.slug)
              return -1;
            if (a.slug > b.slug)
              return 1;
            return 0;
          })
          this.listaCiudades = ciudades
        },
        error => {}
      )
    }
  }

  ionViewWillEnter() {
    this.searchForm["controls"]["restaurantName"].reset()
    this.searchForm["controls"]["restaurantCity"].reset()
    this.searchList = []
    this.showSearch = false
    this.statusBar.backgroundColorByHexString('#e9e9e9')
  }

  genSlug(name) {
    name = name.toLowerCase()
    name = name.normalize('NFD').replace(/[\u0300-\u036f]/g, "")
    name = name.replace(new RegExp(' ', 'g'), '-');
    return name
  }

  buscar() {
    let avoidList: any = []
    if (!this.searchForm.controls.filterVegana.value) avoidList.push('vegana')
    if (!this.searchForm.controls.filterVegetariana.value) avoidList.push('vegetariana')
    if (!this.searchForm.controls.filterOpcion.value) avoidList.push('opcion')
    if (!this.searchForm.controls.filterTienda.value) avoidList.push('tienda')
    if (this.searchForm.controls.restaurantName.value) {
      let list = []
      if (this.searchForm.controls.restaurantCity.value) {
        this.restaurantes.restaurantesNofree.forEach(el => {
          if (el.slug.includes(this.genSlug(this.searchForm.controls.restaurantName.value.trim()))) {
            if (el.ciudad == this.searchForm.controls.restaurantCity.value) {
              let shouldPush: boolean = true
              avoidList.forEach(subEl => {
                if (el.tipo == subEl) shouldPush = false
              })
              if (shouldPush) list.push(el)
            }
          }
        })
      } else {
        this.restaurantes.restaurantesNofree.forEach(el => {
          if (el.slug.includes(this.genSlug(this.searchForm.controls.restaurantName.value.toLowerCase().trim()))) {
            let shouldPush: boolean = true
            avoidList.forEach(subEl => {
              if (el.tipo == subEl) shouldPush = false
            })
            if (shouldPush) list.push(el)
          }
        })
      }
      this.searchList = list
      this.showSearch = true
    } else if (this.searchForm.controls.restaurantCity.value && !this.searchForm.controls.restaurantName.value) {
      let list = []
      this.restaurantes.restaurantesNofree.forEach(el => {
        if (el.ciudad == this.searchForm.controls.restaurantCity.value) {
          let shouldPush: boolean = true
          avoidList.forEach(subEl => {
            if (el.tipo == subEl) shouldPush = false
          })
          if (shouldPush) list.push(el)
        }
      })
      this.searchList = list
      this.showSearch = true
    } else {
      this.showSearch = false
    }
  }

  goToThisRestaurante(restaurante) {
    this.recientesList.forEach((el, i) => {
      if (restaurante.nombre == el.nombre) {
        this.recientesList.splice(i, 1);
      }
    })
    this.recientesList.unshift(restaurante)
    this.storage.set('recientesList', JSON.stringify(this.recientesList))
    this.service.get_perfil_restaurante(restaurante.id).subscribe(
      data => {
        if (data.result == 'ok') {
          this.restaurantes.selectedRestaurante = restaurante
          this.restaurantes.selectedRestauranteProfile = data.perfil_restaurantes[0]
          this.restaurantes.isRestauranteSelected = true
          this.navCtrl.push(RestaurantePage)
        } else {
          let alert = this.alertCtrl.create({
            title: "Error",
            message: "Ha ocurrido un error al cargar el restaurante. Inténtalo de nuevo más tarde.",
            buttons: [{
              text: "OK",
              role: "cancel"
            }]
          })
          alert.present()
        }
      },
      error => {
        let alert = this.alertCtrl.create({
          title: "Error",
          message: "Ha ocurrido un error al cargar el restaurante. Inténtalo de nuevo más tarde.",
          buttons: [{
            text: "OK",
            role: "cancel"
          }]
        })
        alert.present()
      }
    )
  }

  goToMiCuenta() {
    this.navCtrl.push(MiCuentaPage)
  }

  goToInfo() {
    this.navCtrl.push(InfoPage)
  }

}
