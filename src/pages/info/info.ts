import { StatusBar } from '@ionic-native/status-bar';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()

@Component({
  selector: 'page-info',
  templateUrl: 'info.html',
})

export class InfoPage {

  constructor(
    public navCtrl: NavController,
    private statusBar: StatusBar,
  ) {}

  goBack() {
    this.navCtrl.pop()
  }

  ionViewWillEnter() {
    this.statusBar.backgroundColorByHexString('#e9e9e9')
  }

}
