import { ListaPage } from './../lista/lista';
import { MenuPage } from './../menu/menu';
import { FavoritosPage } from './../favoritos/favoritos';
import { BuscarPage } from './../buscar/buscar';
import { MapaPage } from './../mapa/mapa';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, Tabs } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';

@IonicPage()

@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})

export class TabsPage {

  MapaPage = MapaPage
  BuscarPage = BuscarPage
  FavoritosPage = FavoritosPage
  MenuPage = MenuPage
  ListaPage = ListaPage

  @ViewChild('tabs') tabs: Tabs

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private statusBar: StatusBar,
    private events: Events
  ) { }

  ionViewDidLoad() {
    this.events.subscribe('tabs:switch', (tab: number) => {
      this.tabs.select(tab)
    })
  }

  ionViewWillEnter() {
    this.statusBar.backgroundColorByHexString('#e9e9e9')
  }
}