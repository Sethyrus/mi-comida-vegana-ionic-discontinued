import {
  ServiceProvider
} from './../../providers/service/service';
import {
  Component
} from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController,
  Alert
} from 'ionic-angular';
import {
  FormGroup,
  FormBuilder
} from '@angular/forms';
import {
  ProfileProvider
} from '../../providers/profile/profile';
import {
  Storage
} from '@ionic/storage';
import {
  StatusBar
} from '@ionic-native/status-bar';

@IonicPage()

@Component({
  selector: 'page-editar-perfil',
  templateUrl: 'editar-perfil.html',
})

export class EditarPerfilPage {

  form: FormGroup
  listaCiudades: any

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    fb: FormBuilder,
    public profile: ProfileProvider,
    private service: ServiceProvider,
    private storage: Storage,
    private statusBar: StatusBar,
    private alertCtrl: AlertController,
  ) {
    if (profile.tipo == "social") {
      this.form = fb.group({
        name: [profile.username],
        ciudad: [profile.ciudad],
        publicidad: [profile.publicidad],
      })
    } else {
      this.form = fb.group({
        name: [profile.username],
        ciudad: [profile.ciudad],
        email: [profile.email],
        publicidad: [profile.publicidad],
      })
    }
  }

  ionViewDidLoad() {
    if (!this.listaCiudades) {
      this.service.get_ciudades().subscribe(
        data => {
          let ciudades = []
          data.Ciudades.forEach(el => {
            ciudades.push({
              nombre: el.nombre,
              pk: el.pk,
              slug: this.genSlug(el.nombre)
            })
          })
          ciudades = ciudades.sort(function (a, b) {
            if (a.slug < b.slug)
              return -1;
            if (a.slug > b.slug)
              return 1;
            return 0;
          })
          this.listaCiudades = ciudades
        },
        error => {}
      )
    }
  }

  genSlug(name) {
    name = name.toLowerCase()
    name = name.normalize('NFD').replace(/[\u0300-\u036f]/g, "")
    name = name.replace(new RegExp(' ', 'g'), '-');
    return name
  }

  ionViewWillEnter() {
    this.statusBar.backgroundColorByHexString('#66b300')
  }

  submit() {
    let eform: any = this.form.controls

    this.storage.get('token').then((token) => {
      if (!token) {} else {
        this.storage.get('pk').then((pk) => {
          if (!pk) {} else {
            this.service.cambiar_datos(pk, token, {
              nombre: eform.name.value,
              email: this.profile.tipo != 'social' ? eform.email.value : this.profile.email,
              ciudad: eform.ciudad.value,
              publicidad: eform.publicidad.value ? 'true' : 'false'
            }).subscribe(
              data => {
                this.profile.reloadProfile(() => {})
                if (data.result != 'error') {
                  let alert: Alert = this.alertCtrl.create({
                    message: "Tu perfil se ha actualizado correctamente",
                    buttons: [{
                      text: "Ok",
                      role: "cancel",
                    }]
                  })
                  alert.present()
                } else {
                  let alert: Alert = this.alertCtrl.create({
                    message: "Ha ocurrido un error al actualizar tu perfil",
                    buttons: [{
                      text: "Ok",
                      role: "cancel",
                    }]
                  })
                  alert.present()
                }
                this.navCtrl.pop()
              },
              error => {
                this.profile.reloadProfile(() => {})
                let alert: Alert = this.alertCtrl.create({
                  message: "Ha ocurrido un error al actualizar tu perfil",
                  buttons: [{
                    text: "Ok",
                    role: "cancel",
                  }]
                })
                alert.present()
              }
            )
          }
        })
      }
    })
  }
}
