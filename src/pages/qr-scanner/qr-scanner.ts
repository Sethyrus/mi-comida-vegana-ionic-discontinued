import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';

@IonicPage()

@Component({
  selector: 'page-qr-scanner',
  templateUrl: 'qr-scanner.html',
})

export class QrScannerPage {

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private events: Events,
    private statusBar: StatusBar,
  ) {}

  ionViewWillEnter() {
    this.statusBar.backgroundColorByHexString('#66b300')
  }

  ionViewDidLoad() {
    this.events.subscribe('QR:success', (data) => {
      if (data) {
        this.navCtrl.pop()
      }
    })
  }

  ionViewWillLeave() {
    this.events.publish('QR:success', false)
  }

}
