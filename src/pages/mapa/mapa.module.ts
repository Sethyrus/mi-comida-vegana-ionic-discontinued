import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MapaPage } from './mapa';
import { AgmCoreModule } from '@agm/core';

@NgModule({
  declarations: [
    MapaPage,
  ],
  imports: [
    IonicPageModule.forChild(MapaPage),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDQcOvU5p6Q0pv1ebi-5Cs5GiY9YHbCpPQ',
    }),
  ],
})
export class MapaPageModule {}
