import {
  MapaProvider
} from './../../providers/mapa/mapa';
import {
  RestaurantePage
} from './../restaurante/restaurante';
import {
  ServiceProvider
} from './../../providers/service/service';
import {
  LocationProvider
} from './../../providers/location/location';
import {
  InfoPage
} from './../info/info';
import {
  MiCuentaPage
} from './../mi-cuenta/mi-cuenta';
import {
  Component, EventEmitter
} from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  Events,
  AlertController,
  Platform
} from 'ionic-angular';
// import {
//   GoogleMaps,
//   GoogleMap,
//   GoogleMapsEvent,
//   GoogleMapOptions,
//   CameraPosition,
//   MarkerOptions,
//   Marker,
//   Environment,
//   HtmlInfoWindow
// } from '@ionic-native/google-maps';
import {
  RestaurantesProvider
} from '../../providers/restaurantes/restaurantes';
import {
  StatusBar
} from '@ionic-native/status-bar';
import { ProfileProvider } from '../../providers/profile/profile';

@IonicPage()

@Component({
  selector: 'page-mapa',
  templateUrl: 'mapa.html',
})

export class MapaPage {

  map: any
  markers: object = {}
  htmlInfoWindows: any = {}
  isMapLoaded: boolean = false
  restaurantCoincidences: any = {}
  mapStyles: any = [{
    "featureType": "administrative.land_parcel",
    "stylers": [{
      "visibility": "off"
    }]
  },
  {
    "featureType": "administrative.neighborhood",
    "stylers": [{
      "visibility": "off"
    }]
  },
  {
    "featureType": "poi",
    "elementType": "labels",
    "stylers": [{
      "visibility": "off"
    }]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text",
    "stylers": [{
      "visibility": "off"
    }]
  },
  {
    "featureType": "poi.school",
    "elementType": "labels",
    "stylers": [{
      "visibility": "off"
    }]
  },
  {
    "featureType": "poi.sports_complex",
    "stylers": [{
      "visibility": "off"
    }]
  },
  {
    "featureType": "water",
    "elementType": "labels.text",
    "stylers": [{
      "visibility": "off"
    }]
  }
]

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public restaurantes: RestaurantesProvider,
    public location: LocationProvider,
    private statusBar: StatusBar,
    private service: ServiceProvider,
    private alertCtrl: AlertController,
    private platform: Platform,
    private profile: ProfileProvider
  ) {
  }

  ionViewWillEnter() {
    this.statusBar.backgroundColorByHexString('#e9e9e9')
  }

  goToMiCuenta() {
    this.navCtrl.push(MiCuentaPage)
  }

  goToInfo() {
    this.navCtrl.push(InfoPage)
  }
  
  unselectRestaurante() {
    this.restaurantes.selectedMapaRestaurante = ''
  }

  getDirections() {
    let destination = this.restaurantes.selectedMapaRestaurante.ubicacion_x + ',' + this.restaurantes.selectedMapaRestaurante.ubicacion_y;

    if (this.platform.is('ios')) {
      window.open('maps://?q=' + destination, '_system');
    } else {
      let label = encodeURI('My Label');
      window.open('geo:0,0?q=' + destination + '(' + label + ')', '_system');
    }
  }

  getIcon(restaurante) {
    if (restaurante.gratis == 'True') {
      return 'assets/imgs/map-marker(5)a.png'
    } else if (restaurante.gratis == 'False') {
      switch (restaurante.tipo) {
        case 'vegana':
          {
            return 'assets/imgs/map-marker(1)a.png';
          }
        case 'vegetariana':
          {
            return 'assets/imgs/map-marker(2)a.png';
          }
        case 'opcion':
          {
            return 'assets/imgs/map-marker(3)a.png';
          }
        case 'tienda':
          {
            return 'assets/imgs/map-marker(4)a.png';
          }
      }
    }
  }

  goToThisRestaurante(restaurante) {
    console.log('Marker clicked. Restaurant:', restaurante)
    if (this.restaurantes.selectedMapaRestaurante == restaurante) {
      if (restaurante.gratis == "False") {
        this.unselectRestaurante()
        this.service.get_perfil_restaurante(restaurante.id).subscribe(
          data => {
            if (data.result == 'ok') {
              this.restaurantes.selectedRestaurante = restaurante
              this.restaurantes.selectedRestauranteProfile = data.perfil_restaurantes[0]
              this.restaurantes.isRestauranteSelected = true
              this.navCtrl.push(RestaurantePage)
            } else {
              let alert = this.alertCtrl.create({
                title: "Error",
                message: "Ha ocurrido un error al cargar el restaurante. Inténtalo de nuevo más tarde.",
                buttons: [{
                  text: "OK",
                  role: "cancel"
                }]
              })
              alert.present()
            }
          },
          error => {
            let alert = this.alertCtrl.create({
              title: "Error",
              message: "Ha ocurrido un error al cargar el restaurante. Inténtalo de nuevo más tarde.",
              buttons: [{
                text: "OK",
                role: "cancel"
              }]
            })
            alert.present()
          }
        )
      }
    } else {
      this.restaurantes.selectedMapaRestaurante = restaurante
    }
  }
}
