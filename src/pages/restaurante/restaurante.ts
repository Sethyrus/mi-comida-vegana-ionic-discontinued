import {
  QrScannerPage
} from './../qr-scanner/qr-scanner';
import {
  StatusBar
} from '@ionic-native/status-bar';
import {
  ServiceProvider
} from './../../providers/service/service';
import {
  InfoPage
} from './../info/info';
import {
  MiCuentaPage
} from './../mi-cuenta/mi-cuenta';
import {
  Component
} from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController,
  ModalController,
  Events
} from 'ionic-angular';
import {
  RestaurantesProvider
} from '../../providers/restaurantes/restaurantes';
import {
  Storage
} from '@ionic/storage';
import {
  QRScanner,
  QRScannerStatus
} from '@ionic-native/qr-scanner';
import { CallNumber } from '@ionic-native/call-number';
import * as moment from 'moment';

@IonicPage()

@Component({
  selector: 'page-restaurante',
  templateUrl: 'restaurante.html',
})

export class RestaurantePage {

  isRestauranteCartaLoaded: boolean = false

  tipo: any

  showHorario: boolean = false

  horario: any = {}

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public restaurantes: RestaurantesProvider,
    private service: ServiceProvider,
    private storage: Storage,
    private alertCtrl: AlertController,
    private statusBar: StatusBar,
    private qrScanner: QRScanner,
    public modalCtrl: ModalController,
    private events: Events,
    private callNumber: CallNumber,
  ) {
    this.horario = this.horarioData()
    console.log('this.horario', this.horario)
  }

  ionViewDidLoad() {
    if (!this.restaurantes.isRestauranteSelected) {
      this.navCtrl.pop()
    }
  }

  ionViewWillEnter() {
    if (this.restaurantes.selectedRestaurante.tipo == 'vegana') {
      this.statusBar.backgroundColorByHexString('#66b300')
    } else if (this.restaurantes.selectedRestaurante.tipo == 'vegetariana') {
      this.statusBar.backgroundColorByHexString('#d9e021')
    } else if (this.restaurantes.selectedRestaurante.tipo == 'opcion') {
      this.statusBar.backgroundColorByHexString('#fbae17')
    } else if (this.restaurantes.selectedRestaurante.tipo == 'tienda') {
      this.statusBar.backgroundColorByHexString('#80358a')
    }

    this.restaurantes.selectedRestauranteCarta = {}

    this.restaurantes.selectedRestauranteProfile.carta.forEach(el => {
      let exists: boolean = false

      if (!this.restaurantes.selectedRestauranteCarta.categorias || this.restaurantes.selectedRestauranteCarta.categorias.length == 0) {
        this.restaurantes.selectedRestauranteCarta.categorias = []
      }

      if (!this.restaurantes.selectedRestauranteCarta.platos || this.restaurantes.selectedRestauranteCarta.platos.length == 0) {
        this.restaurantes.selectedRestauranteCarta.platos = {}
      }

      if (!this.restaurantes.selectedRestauranteCarta.platos[el.categoria] || this.restaurantes.selectedRestauranteCarta.platos[el.categoria].length == 0) {
        this.restaurantes.selectedRestauranteCarta.platos[el.categoria] = []
      }

      this.restaurantes.selectedRestauranteCarta.categorias.forEach(subEl => {
        if (el.categoria == subEl.name) {
          exists = true
        }
      })

      if (!exists) {
        this.restaurantes.selectedRestauranteCarta.categorias.push({
          name: el.categoria,
          opened: false,
        })
      }

      this.restaurantes.selectedRestauranteCarta.platos[el.categoria].push(el)
    })


    this.isRestauranteCartaLoaded = true
  }

  horarioData() {
    let format = 'hh:mm:ss'

    let horario: any = {
      lunes: {
        mañana: {
          abre: moment(this.restaurantes.selectedRestaurante.horario['lunes'].mañana.h),
          cierra: moment(this.restaurantes.selectedRestaurante.horario['lunes'].mañana.h, format).add(this.restaurantes.selectedRestaurante.horario['lunes'].mañana.t, 'hours'),
        },
        tarde: {
          abre: moment(this.restaurantes.selectedRestaurante.horario['lunes'].tarde.h),
          cierra: moment(this.restaurantes.selectedRestaurante.horario['lunes'].tarde.h, format).add(this.restaurantes.selectedRestaurante.horario['lunes'].tarde.t, 'hours'),
        }
      },
      martes: {
        mañana: {
          abre: moment(this.restaurantes.selectedRestaurante.horario['martes'].mañana.h, format, 'h:mm:ss'),
          cierra: moment(this.restaurantes.selectedRestaurante.horario['martes'].mañana.h, format, 'h:mm:ss').add(this.restaurantes.selectedRestaurante.horario['martes'].mañana.t, 'hours'),
        },
        tarde: {
          abre: moment(this.restaurantes.selectedRestaurante.horario['martes'].mañana.h, format, 'h:mm:ss'),
          cierra: moment(this.restaurantes.selectedRestaurante.horario['martes'].mañana.h, format, 'h:mm:ss').add(this.restaurantes.selectedRestaurante.horario['martes'].mañana.t, 'hours'),
        },
      },
      miercoles: {
        mañana: {
          abre: moment(this.restaurantes.selectedRestaurante.horario['miercoles'].mañana.h, format, 'h:mm:ss'),
          cierra: moment(this.restaurantes.selectedRestaurante.horario['miercoles'].mañana.h, format, 'h:mm:ss').add(this.restaurantes.selectedRestaurante.horario['miercoles'].mañana.t, 'hours'),
        },
        tarde: {
          abre: moment(this.restaurantes.selectedRestaurante.horario['miercoles'].mañana.h, format, 'h:mm:ss'),
          cierra: moment(this.restaurantes.selectedRestaurante.horario['miercoles'].mañana.h, format, 'h:mm:ss').add(this.restaurantes.selectedRestaurante.horario['miercoles'].mañana.t, 'hours'),
        },
      },
      jueves: {
        mañana: {
          abre: moment(this.restaurantes.selectedRestaurante.horario['jueves'].mañana.h, format, 'h:mm:ss'),
          cierra: moment(this.restaurantes.selectedRestaurante.horario['jueves'].mañana.h, format, 'h:mm:ss').add(this.restaurantes.selectedRestaurante.horario['jueves'].mañana.t, 'hours'),
        },
        tarde: {
          abre: moment(this.restaurantes.selectedRestaurante.horario['jueves'].mañana.h, format, 'h:mm:ss'),
          cierra: moment(this.restaurantes.selectedRestaurante.horario['jueves'].mañana.h, format, 'h:mm:ss').add(this.restaurantes.selectedRestaurante.horario['jueves'].mañana.t, 'hours'),
        },
      },
      viernes: {
        mañana: {
          abre: moment(this.restaurantes.selectedRestaurante.horario['viernes'].mañana.h, format, 'h:mm:ss'),
          cierra: moment(this.restaurantes.selectedRestaurante.horario['viernes'].mañana.h, format, 'h:mm:ss').add(this.restaurantes.selectedRestaurante.horario['viernes'].mañana.t, 'hours'),
        },
        tarde: {
          abre: moment(this.restaurantes.selectedRestaurante.horario['viernes'].mañana.h, format, 'h:mm:ss'),
          cierra: moment(this.restaurantes.selectedRestaurante.horario['viernes'].mañana.h, format, 'h:mm:ss').add(this.restaurantes.selectedRestaurante.horario['viernes'].mañana.t, 'hours'),
        },
      },
      sabado: {
        mañana: {
          abre: moment(this.restaurantes.selectedRestaurante.horario['sabado'].mañana.h, format, 'h:mm:ss'),
          cierra: moment(this.restaurantes.selectedRestaurante.horario['sabado'].mañana.h, format, 'h:mm:ss').add(this.restaurantes.selectedRestaurante.horario['sabado'].mañana.t, 'hours'),
        },
        tarde: {
          abre: moment(this.restaurantes.selectedRestaurante.horario['sabado'].mañana.h, format, 'h:mm:ss'),
          cierra: moment(this.restaurantes.selectedRestaurante.horario['sabado'].mañana.h, format, 'h:mm:ss').add(this.restaurantes.selectedRestaurante.horario['sabado'].mañana.t, 'hours'),
        },
      },
      domingo: {
        mañana: {
          abre: moment(this.restaurantes.selectedRestaurante.horario['domingo'].mañana.h, format, 'h:mm:ss'),
          cierra: moment(this.restaurantes.selectedRestaurante.horario['domingo'].mañana.h, format, 'h:mm:ss').add(this.restaurantes.selectedRestaurante.horario['domingo'].mañana.t, 'hours'),
        },
        tarde: {
          abre: moment(this.restaurantes.selectedRestaurante.horario['domingo'].mañana.h, format, 'h:mm:ss'),
          cierra: moment(this.restaurantes.selectedRestaurante.horario['domingo'].mañana.h, format, 'h:mm:ss').add(this.restaurantes.selectedRestaurante.horario['domingo'].mañana.t, 'hours'),
        },
      },
    }
    return horario
  }

  toggleHorario() {
    this.showHorario = !this.showHorario
  }

  toggleCategoria(categoria) {
    this.restaurantes.selectedRestauranteCarta.categorias.forEach(el => {
      if (el.name == categoria) {
        el.opened = !el.opened
      }
    })
  }

  goToMiCuenta() {
    this.navCtrl.push(MiCuentaPage)
  }

  goToInfo() {
    this.navCtrl.push(InfoPage)
  }

  call() {
    let alert = this.alertCtrl.create({
      title: 'Confirmar',
      message: '¿Quieres llamar a ' + this.restaurantes.selectedRestaurante.nombre + ' (' + this.restaurantes.selectedRestauranteProfile.telefono + ')?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
        },
        {
          text: 'Llamar',
          handler: () => {
            this.callNumber.callNumber(this.restaurantes.selectedRestauranteProfile.telefono, false)
          }
        }
      ]
    })
    alert.present()
  }

  goToOpinar() {
    this.storage.get('token').then((token) => {
      this.storage.get('pk').then((pk) => {
        this.service.comprobar_valoracion_abierta(pk, this.restaurantes.selectedRestaurante.id, token).subscribe(
          data => {
            let alert = this.alertCtrl.create({
              title: 'Restaurante no visitado',
              message: 'Para valorar el restaurante debes registrar el código QR solicitado en el propio establecimiento.',
              buttons: [{
                  text: 'Cancelar',
                  role: 'cancel'
                },
                {
                  text: 'Aceptar',
                  handler: () => {
                    alert.dismiss();
                    this.qrScanner.prepare()
                      .then((status: QRScannerStatus) => {
                        if (status.authorized) {
                          this.navCtrl.push(QrScannerPage);
                          (window.document.querySelector('ion-app') as HTMLElement).classList.add('cameraView');
                          this.events.subscribe('QR:success', (data) => {
                            this.events.unsubscribe('QR:success')
                            if (data) {
                              (window.document.querySelector('ion-app') as HTMLElement).classList.remove('cameraView');
                              (window.document.querySelector('ion-app') as HTMLElement).style.backgroundColor = "unset";
                              scanSub.unsubscribe();
                              this.qrScanner.hide();
                              let alert = this.alertCtrl.create({
                                title: "QR terminado",
                                message: data,
                                buttons: [{
                                  text: 'Aceptar',
                                  role: 'cancel'
                                }, ]
                              })
                              alert.present()
                            } else {
                              (window.document.querySelector('ion-app') as HTMLElement).classList.remove('cameraView');
                              (window.document.querySelector('ion-app') as HTMLElement).style.backgroundColor = "unset";
                              scanSub.unsubscribe();
                              this.qrScanner.hide();
                              let alert = this.alertCtrl.create({
                                title: "QR terminado",
                                message: "QR cancelado",
                                buttons: [{
                                  text: 'Aceptar',
                                  role: 'cancel'
                                }, ]
                              })
                              alert.present()
                            }
                          })
                          let scanSub = this.qrScanner.scan().subscribe((text: string) => {
                            this.events.publish('QR:success', text);
                          });

                        } else if (status.denied) {
                          alert.present()
                        } else {}
                      })
                      .catch((e: any) => {});
                  }
                },
              ]
            })
            alert.present()
          },
          error => {
          }
        )
      })
    })
  }

  addFavorito() {
    this.storage.get('token').then((token) => {
      this.storage.get('pk').then((pk) => {
        this.service.registrar_favorito(pk, this.restaurantes.selectedRestaurante.id, token).subscribe(
          data => {
            if (data.message == 'Borrado') {
              this.restaurantes.selectedRestaurante.favorite = false
            } else if (data.message == 'Creado correctamente') {
              this.restaurantes.selectedRestaurante.favorite = true
            }
            this.restaurantes.reloadFavoritos()
          },
          error => {
          }
        )
      })
    })
  }
}
