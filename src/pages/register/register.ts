import {
  LoginPage
} from './../login/login';
import {
  AuthProvider
} from './../../providers/auth/auth';
import {
  NavController,
  AlertController,
  IonicPage
} from 'ionic-angular';
import {
  TabsPage
} from './../../pages/tabs/tabs';
import {
  FormBuilder,
  Validators,
  FormGroup
} from '@angular/forms';
import {
  Component
} from '@angular/core';
import {
  ServiceProvider
} from '../../providers/service/service';

@IonicPage()

@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})

export class RegisterPage {

  registerForm: FormGroup = this.fb.group({
    email: ['', Validators.required],
    name: ['', Validators.required],
    password: ['', Validators.required],
    repeatPassword: ['', Validators.required],
    city: ['', Validators.required],
    policy: [false],
    commercialMailing: [false],
  })

  listaCiudades: any

  ciudad: string

  constructor(
    private fb: FormBuilder,
    private service: ServiceProvider,
    private navCtrl: NavController,
    private alertCtrl: AlertController,
    private authProvider: AuthProvider,
  ) {}

  get passwordsMatch() {
    return (this.registerForm.controls.password.value == this.registerForm.controls.repeatPassword.value)
  }

  volver() {
    this.navCtrl.pop()
  }

  ionViewDidLoad() {
    this.service.get_ciudades().subscribe(
      data => {
        let ciudades = []
        data.Ciudades.forEach(el => {
          ciudades.push({
            nombre: el.nombre,
            pk: el.pk,
            slug: this.genSlug(el.nombre)
          })
        })
        ciudades = ciudades.sort(function (a, b) {
          if (a.slug < b.slug)
            return -1;
          if (a.slug > b.slug)
            return 1;
          return 0;
        })
        this.listaCiudades = ciudades
      },
      error => {}
    )
  }

  genSlug(name) {
    name = name.toLowerCase()
    name = name.normalize('NFD').replace(/[\u0300-\u036f]/g, "")
    name = name.replace(new RegExp(' ', 'g'), '-');
    return name
  }

  register() {
    let usuario = this.registerForm.controls.name.value
    let email = this.registerForm.controls.email.value
    let password = this.registerForm.controls.password.value
    let ciudad = this.registerForm.controls.city.value
    let policy = this.registerForm.controls.policy.value
    let mailing = this.registerForm.controls.commercialMailing.value

    if (usuario.length > 0) {
      if (email.length > 0) {
        if (password.length > 0) {
          if (ciudad.length > 0) {
            if (policy == true) {
              if (this.passwordsMatch) {
                this.service.register(usuario, email, password, ciudad, mailing).subscribe(
                  data => {
                    if (data.result == 'ok') {
                      this.service.login(email, password).subscribe(
                        data => {
                          if (data.result == 'ok') {
                            this.authProvider.setLogin(data.token, data.pk, () => {
                              this.navCtrl.push(TabsPage)
                              this.navCtrl.setRoot(TabsPage)
                            })
                          } else if (data.result == 'error') {
                            let alert = this.alertCtrl.create({
                              title: 'Error',
                              message: "Ha ocurrido un error iniciando tu sesión. Por favor, inténtalo manualmente.",
                              buttons: [{
                                text: 'Aceptar',
                                role: 'cancel',
                                handler: () => {
                                  this.navCtrl.pop()
                                  this.navCtrl.push(LoginPage)
                                }
                              }]
                            })
                            alert.present()
                          }
                        },
                        error => {}
                      )
                    } else if (data.result == 'error') {
                      let alert = this.alertCtrl.create({
                        title: 'Error',
                        message: data.message,
                        buttons: [{
                          text: 'Aceptar',
                          role: 'cancel'
                        }]
                      })
                      alert.present()
                    }
                  },
                  error => {
                    let alert = this.alertCtrl.create({
                      title: 'Error',
                      message: 'Ha ocurrido un error. Vuelve a intentarlo más tarde.',
                      buttons: [{
                        text: 'Aceptar',
                        role: 'cancel'
                      }]
                    })
                    alert.present()
                  }
                )
              } else {
                let alert = this.alertCtrl.create({
                  title: 'Error',
                  message: 'Las contraseñas no coinciden.',
                  buttons: [{
                    text: 'Aceptar',
                    role: 'cancel'
                  }]
                })
                alert.present()
              }
            } else {
              let alert = this.alertCtrl.create({
                title: 'Error',
                message: 'Por favor, lee y acepta la política de privacidad.',
                buttons: [{
                  text: 'Aceptar',
                  role: 'cancel'
                }]
              })
              alert.present()
            }
          } else {
            let alert = this.alertCtrl.create({
              title: 'Error',
              message: 'No has seleccionado tu ciudad.',
              buttons: [{
                text: 'Aceptar',
                role: 'cancel'
              }]
            })
            alert.present()
          }
        } else {
          let alert = this.alertCtrl.create({
            title: 'Error',
            message: 'No has introducido tu contraseña.',
            buttons: [{
              text: 'Aceptar',
              role: 'cancel'
            }]
          })
          alert.present()
        }
      } else {
      let alert = this.alertCtrl.create({
        title: 'Error',
        message: 'No has introducido tu correo electrónico.',
        buttons: [{
          text: 'Aceptar',
          role: 'cancel'
        }]
      })
      alert.present()
      }
    } else {
      let alert = this.alertCtrl.create({
        title: 'Error',
        message: 'No has introducido tu nombre de usuario.',
        buttons: [{
          text: 'Aceptar',
          role: 'cancel'
        }]
      })
      alert.present()
    }
  }
}
