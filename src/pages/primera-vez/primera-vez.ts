import { TabsPage } from './../tabs/tabs';
import { ServiceProvider } from './../../providers/service/service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ProfileProvider } from '../../providers/profile/profile';
import { Storage } from '@ionic/storage';

@IonicPage()

@Component({
  selector: 'page-primera-vez',
  templateUrl: 'primera-vez.html',
})

export class PrimeraVezPage {

  form: FormGroup
  listaCiudades: any

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    fb: FormBuilder,
    private profile: ProfileProvider,
    private service: ServiceProvider,
    private alertCtrl: AlertController,
    private storage: Storage,
  ) {
    this.form = fb.group({
      ciudad: [profile.ciudad],
      publicidad: [profile.publicidad],
    })
  }

  goToTabs() {
    this.storage.get('token').then((token) => {
      if (!token) {
      } else {
        this.storage.get('pk').then((pk) => {
          if (!pk) {
          } else {
            this.service.cambiar_datos(pk, token, {ciudad: this.form.controls.ciudad.value, publicidad: this.form.controls.publicidad.value}).subscribe(
              data => {
                this.profile.reloadProfile(() => {})
              },
              error => { },
            )
          }
        })
      }
    })
    this.navCtrl.push(TabsPage)
    this.navCtrl.setRoot(TabsPage)
    // let alert = this.alertCtrl.create({
    //   message: this.form.controls.ciudad.value
    // })
    // alert.present()
    // this.navCtrl.push(TabsPage)
    // this.navCtrl.setRoot(TabsPage)
  }

  ionViewDidLoad() {
    this.service.get_ciudades().subscribe(
      data => {
        let ciudades = []
        data.Ciudades.forEach(el => {
          ciudades.push({
            nombre: el.nombre,
            pk: el.pk,
            slug: this.genSlug(el.nombre)
          })
        })
        ciudades = ciudades.sort(function (a, b) {
          if (a.slug < b.slug)
            return -1;
          if (a.slug > b.slug)
            return 1;
          return 0;
        })
        this.listaCiudades = ciudades
      },
      error => {}
    )
  }

  genSlug(name) {
    name = name.toLowerCase()
    name = name.normalize('NFD').replace(/[\u0300-\u036f]/g, "")
    name = name.replace(new RegExp(' ', 'g'), '-');
    return name
  }

}
