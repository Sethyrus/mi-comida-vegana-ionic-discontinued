import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PrimeraVezPage } from './primera-vez';

@NgModule({
  declarations: [
    PrimeraVezPage,
  ],
  imports: [
    IonicPageModule.forChild(PrimeraVezPage),
  ],
})
export class PrimeraVezPageModule {}
