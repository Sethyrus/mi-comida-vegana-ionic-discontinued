import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()

@Component({
  selector: 'page-dejar-opinion',
  templateUrl: 'dejar-opinion.html',
})

export class DejarOpinionPage {

  calidadValue: number = 0
  tiempoValue: number = 0
  comidaValue: number = 0
  text: string = ''

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
  ) { }

  setValue(type, value) {
    switch(type) {
      case 'calidad': {
        this.calidadValue = value
        break
      }
      case 'tiempo': {
        this.tiempoValue = value
        break
      }
      case 'comida': {
        this.comidaValue = value
        break
      }
    }
  }

  submit() {
    console.log('calidadValue: ' + this.calidadValue + '  this.tiempoValue: ' + this.tiempoValue + ' this.comidaValue: ' + this.comidaValue + ' text: ' + this.text)
  }

}
