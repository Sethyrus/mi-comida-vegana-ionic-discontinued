import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DejarOpinionPage } from './dejar-opinion';

@NgModule({
  declarations: [
    DejarOpinionPage,
  ],
  imports: [
    IonicPageModule.forChild(DejarOpinionPage),
  ],
})
export class DejarOpinionPageModule {}
